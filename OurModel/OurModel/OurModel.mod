tuple Arc {
	int start;
	int stop;	
}

int k = ...; // Number of required paths
int n = ...; // Number of nodes
int p = ...; // Amount of predefined paths

int SourceNode = ...;
int DestinationNode = ...;

range NodeRange = 1..n;
range PredefinedPathRange = 1..p;

{Arc} Arcs with start in NodeRange, stop in NodeRange = ...; //  // Connections between nodes: should be bidirectional
float Cost[Arcs] = ...;  // Cost of the connection - bidirectional

int ConstraintsNum = ...;	// Number of constraints
range ConstraintsRange = 1..ConstraintsNum;

float U[ConstraintsRange] = ...; // Weight limits for each Constraint
float W[ConstraintsRange][Arcs] = ...; // Weight of each node for each dimension

int PotentialPaths[PredefinedPathRange][Arcs] = ...;

dvar boolean x[PredefinedPathRange];

minimize
    sum(r in PredefinedPathRange, e in Arcs) Cost[e] * PotentialPaths[r][e] * x[r];

subject to {
	REDUNDANCY_ENSURED:
		k <= p;

	TOTAL_USED:
		sum(i in PredefinedPathRange) x[i] == k;
	
	WEIGHT_LIMITS:
		forall(q in PredefinedPathRange, i in ConstraintsRange) {		
			 x[q] * sum(j in Arcs) W[i][j] * PotentialPaths[q][j] <= U[i];		
		}
}

{Arc} EdgesUsed[p in PredefinedPathRange] = {i | i in Arcs: PotentialPaths[p][i] > 0 };

execute {
	writeln("Total cost: " + cplex.getObjValue());
	for(var i in x) {
		if(x[i] == 1) {	
			writeln(EdgesUsed[i]);
		}		
	}
}