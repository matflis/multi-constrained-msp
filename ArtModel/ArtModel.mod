
main {
	// Model sources
	var mscpModelSource = new IloOplModelSource("MscpModel.mod");
	
	// Model variables
	var mscpCplexModel = new IloCplex();
	
	// Model definitions 
	var mscpModelDefinition = new IloOplModelDefinition(mscpModelSource);
	
	// Opl Model	
	var shortestPathOplModel = new IloOplModel(mscpModelDefinition, mscpCplexModel);
	
	// Data source
	var mscpDataSource = new IloOplDataSource("DataSource.dat");

	// Copy data	
	var mscpDataElements = new IloOplDataElements();		
	
	shortestPathOplModel.addDataSource(mscpDataSource);
	shortestPathOplModel.generate();
	
	if(mscpCplexModel.solve()) {
		writeln("Limity wynosily:");	
		for(var i = 1; i <= shortestPathOplModel.ConstraintsNum; i++) {
			write(shortestPathOplModel.U[i] + " ");		
		}
		writeln();
		
		for(var i = 1; i <= shortestPathOplModel.k; i++) {
			writeln("Sciezka nr: " + i + " uzywa krawedzi: " + shortestPathOplModel.EdgesUsed[i]);
			for(var p in shortestPathOplModel.W) {	
				var cost = 0;
				for(var q in shortestPathOplModel.EdgesUsed[i]) {
					cost = cost + shortestPathOplModel.W[p][q];
				}
				writeln("Ograniczenie: " + p + ", zuzycie: " + cost);
  			}
  			var pathCost = 0;
  			for(var p in shortestPathOplModel.EdgesUsed[i]) {
  				pathCost = pathCost + shortestPathOplModel.Cost[p];		  			
  			}
  			writeln("Total cost: " + pathCost);
		}			
	}
	else {
		writeln("Nie znaleziono rozwiazania");
	}
		
	shortestPathOplModel.end();
	mscpDataSource.end();
	mscpModelSource.end();
	mscpCplexModel.end();
	mscpModelDefinition.end();
}