tuple Arc {
	int start;
	int stop;
}

int k = ...; // Number of paths
int n = ...;	// Number of nodes
int SourceNode = ...;
int DestinationNode = ...;

range NodeRange = 1..n;
range PathRange = 1..k;

{Arc} Arcs with start in NodeRange, stop in NodeRange = ...; // Connections between nodes: should be bidirectional
float Cost[Arcs] = ...;	// Cost of the connection - bidirectional

int ConstraintsNum = ...;	// Number of constraints
range ConstraintsRange = 1..ConstraintsNum;

float U[ConstraintsRange] = ...; // Weight limits for each Constraint
float W[ConstraintsRange][Arcs] = ...; // Weight of each node for each dimension

dvar int+ x[PathRange][Arcs];	// Decision arcs variable

minimize
	sum(j in PathRange, i in Arcs) Cost[i] * x[j][i];

subject to {
	WEIGHT_LIMITS:
		forall(p in PathRange, i in ConstraintsRange) {
			sum(j in Arcs) W[i][j] * x[p][j] <= U[i];		
		}
	
	IN_OUT:
		forall(p in PathRange, i in NodeRange: i != SourceNode && i != DestinationNode) {
			sum(j in Arcs: j.start == i) x[p][j] - sum(j in Arcs: j.stop == i) x[p][j] == 0;		
		}
		
	ONE_LEAVING:
		forall(p in PathRange) 
			sum(i in Arcs: i.start == SourceNode) x[p][i] == 1;
	
	ONE_ENTERING:
		forall(p in PathRange) 
			sum(i in Arcs: i.stop == DestinationNode) x[p][i] == 1;
		
	EITHER_OR:
		forall(p in PathRange, i in Arcs) x[p][<i.start, i.stop>] + x[p][<i.stop, i.start>] <= 1;
		
	EXCLUDING_PATHS:
		forall(p in PathRange, q in PathRange: p < q) {
			sum(i in Arcs) x[p][i] * x[q][i] <= sum(i in Arcs) x[p][i] - 1;
		}
		
	BINARY_VALUE:
		forall(p in PathRange, i in Arcs) x[p][i] <= 1;		
}

{Arc} EdgesUsed[k in PathRange] = {i | i in Arcs: x[k][i] > 0 };

execute {
	writeln("Total cost: " + cplex.getObjValue());
	writeln(EdgesUsed);
}