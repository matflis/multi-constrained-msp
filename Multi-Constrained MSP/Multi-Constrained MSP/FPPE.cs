﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Multi_Constrained_MSP.Graph;
using Multi_Constrained_MSP.HelperVariables;
using Multi_Constrained_MSP.Utilities;

namespace Multi_Constrained_MSP
{
    public static class StopWatchHolder
    {
        public static readonly Stopwatch _stopWatch = new Stopwatch();

    }

    public static class FPPE
    {
        private static List<string> checklist = new List<string>();
        public static void CalculateFPPE(Node source, Node destination, int J, out bool solved)
        {
            solved = false;

            foreach (var node in Program.Nodes)
            {
                var tmpArc = node.Arcs.ToList();
                tmpArc = tmpArc.OrderByDescending(a => a.Cost).Reverse().ToList();
                node.Arcs = tmpArc.ToList();
            }

            var P = new List<Path>();
            var Q = Program.P1.ToList();

            while (Q.Any())
            {
                Q.RemoveAll(qCost => qCost.PathCost < 0);
                foreach (var qp in Q)
                {
                    if(P.Any(p => p.PathId == qp.PathId))
                        qp.PathCost = int.MaxValue;
                }
                Q.RemoveAll(qCost => qCost.PathCost == int.MaxValue);
                if (!Q.Any())
                    break;

                var sorted = Q.OrderBy(p => p.PathCost).ToList();
                var q = sorted[0];
                Q.RemoveAt(0);
                if (!GraphUtilities.CheckIfExeedConstraints(q, Program.WeightsLimits))
                {
                    if (P.All(p => p.PathId != q.PathId))
                    {
                        P.Add(q);
                        Console.WriteLine("Path: " + q + " Added.");
                        if (P.Count >= J)
                        {
                            solved = true;
                            Program.P1 = P;
                            return;
                        }
                    }
                }

                var dqe = GraphUtilities.Yens(q.Hops[1], destination, 2);
                Program.Nodes = GraphUtilities.DeepClone(Program.GraphBackup);
                var dqeCandidate = new Path();

                foreach (var p in dqe)
                {
                    if (Q.Any(potentialPath => potentialPath.PathId == p.PathId))
                        continue;

                    if (P.Any(potentialPath => potentialPath.PathId == p.PathId))
                        continue;

                    var j = 1;
                    dqeCandidate = p;
                    while (j < dqeCandidate.Hops.Count)
                    {
                        if (dqeCandidate.Hops[j].NodeId == destination.NodeId)
                        {
                            j++;
                            continue;
                        }

                        var tmpHops = dqeCandidate.Hops.ToList();
                        dqeCandidate.Hops = tmpHops.Skip(j).ToList();
                        var spaths = GraphUtilities.YensCore(dqeCandidate, destination, 2);

                        dqeCandidate.Hops = tmpHops.ToList();
                        Program.Nodes = GraphUtilities.DeepClone(Program.GraphBackup);

                        if (spaths.Count < 2)
                        {
                            j++;
                            continue;
                        }

                        foreach (var spath in spaths)
                        {
                            if (!spaths.Any())
                            {
                                j++;
                                continue;
                            }

                            var newPath = new Path();

                            if (spath.Hops.First().NodeId == source.NodeId)
                            {
                                newPath = spath;
                            }
                            else
                            {
                                var newHops = new List<Node> { q.Hops[0] };
                                newHops.AddRange(dqeCandidate.Hops.Take(j).ToList());
                                newHops.AddRange(spaths[1].Hops);

                                newPath = new Path
                                {
                                    Hops = newHops.ToList(),
                                    EndNode = destination,
                                    StartingNode = newHops.First()
                                };
                                newPath.CalculatePathCost();
                            }

                            if (Q.Any(potentialPath => potentialPath.PathId == newPath.PathId))
                            {
                                j++;
                                continue;
                            }

                            if (P.Any(potentialPath => potentialPath.PathId == newPath.PathId))
                            {
                                j++;
                                continue;
                            }

                            if (!GraphUtilities.CheckIfExeedConstraints(newPath, Program.WeightsLimits))
                            {
                                Q.Add(newPath);
                            }
                        }

                        j++;
                    }
                }
            }

            Program.P1 = P;
        }
    }
}