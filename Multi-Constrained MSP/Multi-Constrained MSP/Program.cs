﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Multi_Constrained_MSP.Graph;
using Multi_Constrained_MSP.Utilities;
using Path = Multi_Constrained_MSP.Graph.Path;

namespace Multi_Constrained_MSP
{
    internal class Program
    {
        public static Dictionary<int, int[]> Connections = new Dictionary<int, int[]>
        {
            {1, new[] {2, 3}},
            {2, new[] {1, 3, 6}},
            {3, new[] {1, 2, 4, 6}},
            {4, new[] {3, 5}},
            {5, new[] {4, 6}},
            {6, new[] {2, 3, 5}}
        };

        public static Dictionary<Tuple<int, int>, int> Costs = new Dictionary<Tuple<int, int>, int>
        {
            {new Tuple<int, int>(1, 2), 5},
            {new Tuple<int, int>(1, 3), 6},
            {new Tuple<int, int>(2, 3), 4},
            {new Tuple<int, int>(2, 6), 2},
            {new Tuple<int, int>(3, 6), 4},
            {new Tuple<int, int>(3, 4), 5},
            {new Tuple<int, int>(4, 5), 6},
            {new Tuple<int, int>(6, 5), 12}
        };

        public static Dictionary<Tuple<int, int>, int[]> Weights = new Dictionary<Tuple<int, int>, int[]>
        {
            {new Tuple<int, int>(1, 2), new[] {1, 2, 3}},
            {new Tuple<int, int>(1, 3), new[] {2, 3, 5}},
            {new Tuple<int, int>(2, 3), new[] {7, 3, 2}},
            {new Tuple<int, int>(2, 6), new[] {3, 2, 4}},
            {new Tuple<int, int>(3, 6), new[] {2, 3, 4}},
            {new Tuple<int, int>(3, 4), new[] {3, 5, 4}},
            {new Tuple<int, int>(4, 5), new[] {4, 7, 1}},
            {new Tuple<int, int>(6, 5), new[] {20, 3, 8}}
        };

        public static int[] WeightsLimits =
        {
            200,
            250,
            300
        };

        public static List<Node> Nodes = new List<Node>();
        public static double ConnectionProbability;
        public static List<int> Lambda;
        public static List<Path> P1;

        public static void LoadNodes(int nodesCount, bool example = false)
        {
            if (!example)
                for (var i = 0; i < nodesCount; i++)
                    Nodes.Add(new Node {NodeId = i+1});
           	else
               	foreach (var connection in Connections)
                	Nodes.Add(new Node { NodeId = connection.Key });
        }

		public static void FillNodes(bool example = false)
        {
			if (!example)
			{
				var rnd = new Random();
				foreach (var node in Nodes)
				{
					foreach (var nei in Nodes)
					{
						if (node == nei)
							continue;

						if (node.Arcs.Any(a => a.Destination == nei))
							continue;

						var prob = rnd.NextDouble();
						if (!(prob <= ConnectionProbability))
							continue;

						var cost = rnd.Next(1, 50);
						var arcWeights = new[] { rnd.Next(1, 50), rnd.Next(1, 50), rnd.Next(1, 50) };

                        node.Arcs.Add(new Arc
						{
							ArcId = node.NodeId + "<->" + nei.NodeId,
							Cost = cost,
							Destination = nei,
							ArcWeights = arcWeights
						});
						nei.Arcs.Add(new Arc
						{
							ArcId = nei.NodeId + "<->" + node.NodeId,
							Cost = cost,
							Destination = node,
							ArcWeights = arcWeights
						});
					}
				}
			}
			else
			{
				foreach (var node in Nodes)
					foreach (var nei in Connections[node.NodeId])
						node.Arcs.Add(new Arc
						{
							ArcId = node.NodeId + "<->" + nei,
							Cost = Costs.GetCost(node.NodeId, nei),
							Destination = Nodes.Find(n => n.NodeId == nei),
							ArcWeights = Weights.GetWeights(node.NodeId, nei)
						});
			}
        }

        public static void PrintNodes()
        {
            foreach (var node in Nodes)
            {
                Console.WriteLine("Badamy Node " + node.NodeId + ":");
                foreach (var arc in node.Arcs)
                {
                    Console.WriteLine("Przypisany string nazwy: " + arc.ArcId);
                    Console.WriteLine("Wenzeł końcowy: " + arc.Destination.NodeId);
                    Console.WriteLine("Wagi połączenia: ");
                    for (var i = 0; i < arc.ArcWeights.Length; i++)
                    {
                        Console.Write(arc.ArcWeights[i]);
                        if (i < arc.ArcWeights.Length - 1)
                            Console.Write("/");
                    }
                    Console.Write("\n");
                    Console.WriteLine("Koszt łącza: " + arc.Cost);
                }
                Console.WriteLine();
            }
        }

        public static List<Path> FindFP(int source, int destination, int R, int J)
        {
            bool solved;
            FPDT.CalculateFPDT(Nodes.Find(n => n.NodeId == source), Nodes.Find(n => n.NodeId == destination), J, out solved);
            if (!solved)
                FPPE.CalculateFPPE(Nodes.Find(n => n.NodeId == source), Nodes.Find(n => n.NodeId == destination), J, out solved);

            P1 = P1.OrderBy(p => p.PathCost).ToList();

            if (solved)
            {
                Console.WriteLine("Znaleziono rozwiazania: ");
                P1.ForEach(p => Console.WriteLine($"{p} (Cost: {p.PathCost}) U = ({string.Join(",", p.PathConstraintCost)})"));
            }
            else
            {
                Console.WriteLine("Brak rozwiazan");
                P1.ForEach(p => Console.WriteLine($"{p} (Cost: {p.PathCost}) U = ({string.Join(",", p.PathConstraintCost)})"));
            }

            return P1;
        }

        private static void ConvertToCPLEXData(int constraints, int k, int source, int destination)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("k = " + k + ";");
            builder.AppendLine("n = " + Nodes.Count + ";");
            builder.AppendLine("SourceNode = " + source + ";");
            builder.AppendLine("DestinationNode = " + destination + ";");
            builder.AppendLine("ConstraintsNum = " + constraints + ";");

            builder.AppendLine("U = [" + string.Join(", ", WeightsLimits.Select(s => $"{s}").ToList()) + "];");

            builder.AppendLine("Arcs = {" + Environment.NewLine);
            foreach (var node in Nodes)
            {
                if (!node.Arcs.Any())
                    continue;
                builder.AppendLine("// Node " + node.NodeId + " Arcs");
                builder.Append("\t" + string.Join(", ", node.Arcs.Select(s => $"<{node.NodeId}, {s.Destination.NodeId}>").ToList()));
                if (node.NodeId != Nodes.Count)
                    builder.Append(",");
                builder.AppendLine(Environment.NewLine);
            }
            builder.AppendLine("};");
            builder.AppendLine(Environment.NewLine);
            builder.AppendLine("Cost = [" + Environment.NewLine);
            foreach (var node in Nodes)
            {
                if (!node.Arcs.Any())
                    continue;
                builder.AppendLine("// Node " + node.NodeId + " Costs");
                builder.Append("\t" + string.Join(", ", node.Arcs.Select(s => $"{s.Cost}").ToList()));
                if (node.NodeId != Nodes.Count)
                    builder.Append(",");
                builder.AppendLine(Environment.NewLine);
            }
            builder.AppendLine("];");

            builder.AppendLine(Environment.NewLine);
            builder.AppendLine("W = [");
            for (var r = 0; r < WeightsLimits.Length; r++)
            {
                builder.AppendLine("\t[");
                foreach (var node in Nodes)
                {
                    if (!node.Arcs.Any())
                        continue;
                    builder.AppendLine("\t// Node " + node.NodeId + " Constraints");
                    builder.Append("\t\t" + string.Join(", ", node.Arcs.Select(s => $"{s.ArcWeights[r]}").ToList()));
                    if (node.NodeId != Nodes.Count)
                        builder.Append(",");
                    builder.AppendLine(Environment.NewLine);
                }
                builder.AppendLine(r < WeightsLimits.Length-1 ? "\t]," : "\t]");
            }
            builder.AppendLine("];");

            using (StreamWriter writer = new StreamWriter(@"CPLEXDATA.txt"))
            {
                writer.WriteLine(builder.ToString());
            }
        }

        private static void ConvertToOurModelCPLEXData(List<Path> paths, int constraints, int k, int sourceId, int destinationId)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine("k = " + k + ";");
            builder.AppendLine("n = " + Nodes.Count + ";");
            builder.AppendLine("p = " + paths.Count + ";");
            builder.AppendLine("SourceNode = " + sourceId + ";");
            builder.AppendLine("DestinationNode = " + destinationId + ";");
            builder.AppendLine("ConstraintsNum = " + constraints + ";");

            builder.AppendLine("U = [" + string.Join(", ", WeightsLimits.Select(s => $"{s}").ToList()) + "];");

            // Arcs
            builder.AppendLine("Arcs = {" + Environment.NewLine);
            foreach (var node in Nodes)
            {
                if (!node.Arcs.Any())
                    continue;
                builder.AppendLine("// Node " + node.NodeId + " Arcs");
                builder.Append("\t" + string.Join(", ", node.Arcs.Select(s => $"<{node.NodeId}, {s.Destination.NodeId}>").ToList()));
                if (node.NodeId != Nodes.Count)
                    builder.Append(",");
                builder.AppendLine(Environment.NewLine);
            }
            builder.AppendLine("};");
            builder.AppendLine(Environment.NewLine);

            // Costs
            builder.AppendLine("Cost = [" + Environment.NewLine);
            foreach (var node in Nodes)
            {
                if (!node.Arcs.Any())
                    continue;
                builder.AppendLine("// Node " + node.NodeId + " Costs");
                builder.Append("\t" + string.Join(", ", node.Arcs.Select(s => $"{s.Cost}").ToList()));
                if (node.NodeId != Nodes.Count)
                    builder.Append(",");
                builder.AppendLine(Environment.NewLine);
            }
            builder.AppendLine("];");

            builder.AppendLine(Environment.NewLine);
            // Weights
            builder.AppendLine("W = [");
            for (var r = 0; r < WeightsLimits.Length; r++)
            {
                builder.AppendLine("\t[");
                foreach (var node in Nodes)
                {
                    if (!node.Arcs.Any())
                        continue;
                    builder.AppendLine("\t// Node " + node.NodeId + " Constraints");
                    builder.Append("\t\t" + string.Join(", ", node.Arcs.Select(s => $"{s.ArcWeights[r]}").ToList()));
                    if (node.NodeId != Nodes.Count)
                        builder.Append(",");
                    builder.AppendLine(Environment.NewLine);
                }
                builder.AppendLine(r < WeightsLimits.Length - 1 ? "\t]," : "\t]");
            }
            builder.AppendLine("];");
            builder.AppendLine(Environment.NewLine);
            
            // Potential paths
            builder.AppendLine("PotentialPaths = [ ");
            for(var r = 0; r < paths.Count; r++)
            {
                builder.AppendLine("\t[");
                for(var i = 0; i < Nodes.Count; i++)
                {
                    builder.Append("\t\t");
                    for (var a = 0; a < Nodes[i].Arcs.Count; a++) {
                        builder.Append(PathIncludes(paths[r], Nodes[i].NodeId, Nodes[i].Arcs[a].Destination.NodeId) ? " 1" : " 0");
                        builder.Append((i == Nodes.Count - 1 && a == Nodes[i].Arcs.Count - 1) ? "" : ",");
                    }
                    builder.AppendLine(Environment.NewLine);
                }
                builder.AppendLine(r < WeightsLimits.Length - 1 ? "\t]," : "\t]");
            }
            builder.AppendLine("];");


            using (StreamWriter writer = new StreamWriter(@"OWN_CPLEXDATA.txt"))
            {
                writer.WriteLine(builder.ToString());
            }
        }

        private static bool PathIncludes(Path path, int sourceId, int destinationId)
        {
            for(var i = 0; i < path.Hops.Count - 1; i++)
            {
                if(path.Hops[i].NodeId == sourceId && path.Hops[i + 1].NodeId == destinationId)
                {
                    return true;
                }
            }
            return false;
        }

        public static List<Node> GraphBackup = new List<Node>();

        private static void Main(string[] args)
        {
            var rnd = new Random();
            var nodeCount = 50;
            ConnectionProbability = 0.15;
            var k = 6;
            var j = 100;
            var constraints = 3;

			bool example = false;

            LoadNodes(nodeCount, example);
            FillNodes(example);

			Node source;
			Node destination;

			if (!example)
			{
				source = new Node();
				destination = new Node();
				while (source.Arcs.Count == 0)
				{
					source = Nodes[rnd.Next(0, nodeCount)];
				}
				while (destination.Arcs.Count == 0)
				{
					destination = Nodes[rnd.Next(0, nodeCount)];
				}
			}
			else
			{
			    WeightsLimits = new[] {20, 20, 20};
				source = Nodes.Find(n => n.NodeId == 1);
				destination = Nodes.Find(n => n.NodeId == 5);
			}

            var paths = PathsFinder.FindPaths(source, destination, j);
            var osolution = new OurSolution();
            GraphBackup = GraphUtilities.DeepClone(Nodes);
            ConvertToCPLEXData(constraints, k, source.NodeId, destination.NodeId);
            ConvertToOurModelCPLEXData(paths, constraints, k, source.NodeId, destination.NodeId);

            PrintNodes();
            Console.WriteLine("Their solution - START");
            StopWatchHolder._stopWatch.Start();
            FindFP(source.NodeId, destination.NodeId, constraints, k);
            StopWatchHolder._stopWatch.Stop();
            Console.WriteLine("Elapsed: " + StopWatchHolder._stopWatch.Elapsed);
            Console.WriteLine("Their solution - ENDS");
            Console.WriteLine();
            Console.WriteLine("Our solution - START");
            Nodes = GraphUtilities.DeepClone(GraphBackup);
            osolution.Solve(source, destination, k);
            Console.ReadLine();
        }
    }
}