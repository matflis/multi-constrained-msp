﻿using System;
using System.Linq;
using Multi_Constrained_MSP.Graph;
using Multi_Constrained_MSP.Utilities;

namespace Multi_Constrained_MSP
{
    public static class FPDT
    {
        public static void CalculateFPDT(Node source, Node destination, int J, out bool solved)
        {
            solved = false;
            var K = Math.Max(20, 2*J);
            var P = GraphUtilities.Yens(source, destination, K);
            GraphUtilities.FlagOutOfConstraintsPaths(P, Program.WeightsLimits);
            P.RemoveAll(p => p.PathCost == int.MaxValue);

            if (P.Count >= J)
            {
                solved = true;
            }

            Program.P1 = P.ToList();
        }
    }
}