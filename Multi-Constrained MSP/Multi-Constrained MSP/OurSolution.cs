﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Constrained_MSP.Graph;
using Multi_Constrained_MSP.Utilities;

namespace Multi_Constrained_MSP
{
    public class OurSolution
    {
        public void Solve(Node source, Node destination, int k)
        {
            Console.WriteLine("Wyliczenie J sciezek za pomoca algorytmu Yens'a dla trasy: " + source.NodeId + " -> " + destination.NodeId);

            StopWatchHolder._stopWatch.Start();

            var shortestConstraintPaths = GraphUtilities.ConstraintYens(source, destination, k, Program.WeightsLimits);

            StopWatchHolder._stopWatch.Stop();

            Console.WriteLine();
            Console.WriteLine("Globalne limity: " + string.Join(",", Program.WeightsLimits));
            shortestConstraintPaths.ForEach(p => Console.WriteLine($"{p} (Cost: {p.PathCost}) U = ({string.Join(",", p.PathConstraintCost)})"));
            Console.WriteLine("\nCzas trwania algorytmu: " + StopWatchHolder._stopWatch.Elapsed);
        }
    }
}
