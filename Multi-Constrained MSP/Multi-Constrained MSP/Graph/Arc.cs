﻿namespace Multi_Constrained_MSP.Graph
{
    public class Arc
    {
        public string ArcId { get; set; }
        public Node Destination { get; set; }
        public int[] ArcWeights { get; set; }
        public int Cost { get; set; }

        public override string ToString()
        {
            return ArcId;
        }
    }
}