﻿using System.Collections.Generic;
using System.Linq;

namespace Multi_Constrained_MSP.Graph
{
    public class Node
    {
        public int NodeId { get; set; }
        public List<Arc> Arcs { get; set; } = new List<Arc>();

        public override string ToString()
        {
            return "Node"+NodeId;
        }
    }
}