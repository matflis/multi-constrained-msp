﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Policy;

namespace Multi_Constrained_MSP.Graph
{
    public class Path
    {
        public string PathId
        {
            get
            {
                if (StartingNode == null)
                    StartingNode = Hops.First();

                return $"{string.Join(" -> ", Hops.Select(node => node.NodeId))}";
            }
        }
        public Node StartingNode { get; set; }
        public Node EndNode { get; set; }
        public List<Node> Hops { get; set; } = new List<Node>();
        public int PathCost { get; set; } = int.MaxValue;
        public int[] PathConstraintCost { get; } = new int[Program.WeightsLimits.Length];

        public void CalculatePathCost()
        {
            if (StartingNode == null)
                StartingNode = Hops.First();

            if (StartingNode.NodeId == EndNode.NodeId)
            {
                PathCost = int.MaxValue;
                foreach (var pcs in PathConstraintCost)
                    PathConstraintCost[pcs] = int.MaxValue;

                return;
            }

            if (Hops.Count <= 1)
            {
                PathCost = int.MaxValue;
                return;
            }

            var cost = 0;
            for (var i = 0; i < Hops.Count; i++)
            {
                var foundArc = Hops[i].Arcs.Find(a => a.Destination.NodeId == Hops[i + 1].NodeId);
                
                cost += foundArc?.Cost ?? int.MaxValue;
                for (var cs = 0; cs < foundArc?.ArcWeights.Length; cs++)
                {
                    PathConstraintCost[cs] += foundArc?.ArcWeights[cs] ?? int.MaxValue;
                }

                if (Hops[i+1].NodeId == Hops.Last().NodeId)
                    break;
            }
            PathCost = cost;
        }

        public static bool IsEquals(List<Node> p1, List<Node> p2)
        {
            if (p1.Count == 0 || p2.Count == 0)
                return false;

            if (p1.Count != p2.Count)
                return false;

            return !p1.Where((t, i) => t.NodeId != p2[i].NodeId).Any();
        }

        public override bool Equals(object obj)
        {
            var other = obj as Path;
            // Yebać
            return IsEquals(this.Hops, other.Hops);
        }

        public override int GetHashCode()
        {
            return 1;
        }

        public override string ToString()
        {
            if (StartingNode == null)
                StartingNode = Hops.First();
            return $"({StartingNode.NodeId} => {EndNode.NodeId}) : {PathId}";
        }
    }
}