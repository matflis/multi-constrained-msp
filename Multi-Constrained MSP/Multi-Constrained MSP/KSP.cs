﻿using System.Collections.Generic;
using System.Linq;
using Multi_Constrained_MSP.Graph;

namespace Multi_Constrained_MSP
{
    public static class KSP
    {
        public static List<Path> GetKSP(this List<Node> nodes, int k)
        {
            var paths = new List<Path>
            {
                new Path
                {
                    Hops = new List<Node>
                    {
                        nodes.Find(n => n.NodeId == 1),
                        nodes.Find(n => n.NodeId == 2),
                        nodes.Find(n => n.NodeId == 6),
                        nodes.Find(n => n.NodeId == 5)
                    }
                },
                new Path
                {
                    Hops = new List<Node>
                    {
                        nodes.Find(n => n.NodeId == 1),
                        nodes.Find(n => n.NodeId == 3),
                        nodes.Find(n => n.NodeId == 6),
                        nodes.Find(n => n.NodeId == 5)
                    }
                },
                new Path
                {
                    Hops = new List<Node>
                    {
                        nodes.Find(n => n.NodeId == 1),
                        nodes.Find(n => n.NodeId == 2),
                        nodes.Find(n => n.NodeId == 3),
                        nodes.Find(n => n.NodeId == 4),
                        nodes.Find(n => n.NodeId == 5)
                    }
                },
                new Path
                {
                    Hops = new List<Node>
                    {
                        nodes.Find(n => n.NodeId == 1),
                        nodes.Find(n => n.NodeId == 3),
                        nodes.Find(n => n.NodeId == 2),
                        nodes.Find(n => n.NodeId == 6),
                        nodes.Find(n => n.NodeId == 5)
                    }
                }
            };

            return paths.Take(k).ToList();
        }
    }
}