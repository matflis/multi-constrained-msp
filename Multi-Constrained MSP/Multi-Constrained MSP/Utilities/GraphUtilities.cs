﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Multi_Constrained_MSP.Graph;

namespace Multi_Constrained_MSP.Utilities
{
	public static class GraphUtilities
	{
		private static readonly Dictionary<int, Tuple<int, int>> DijkstraData = new Dictionary<int, Tuple<int, int>>();
		private static readonly Dictionary<int, bool> CheckList = new Dictionary<int, bool>();
		public static Path Dijkstra(Node source, Node destination)
		{
		    var workCopy = DeepClone(Program.Nodes);

			CalculateDijkstra(source);

			var usedSource = destination;
		    var usedPath = new Path
		    {
		        StartingNode = source,
		        EndNode = destination
		    };

		    usedPath.Hops.Add(usedSource);

			var currentHop = DijkstraData[usedSource.NodeId];

			while (currentHop.Item2 != 0 && currentHop.Item1 != -1)
			{
				usedPath.Hops.Add(workCopy.Find(n => n.NodeId == currentHop.Item1));
				currentHop = DijkstraData[currentHop.Item1];
			}

			usedPath.Hops.Reverse();
			usedPath.PathCost = DijkstraData[usedSource.NodeId].Item2;

			return usedPath;
		}

	    public static List<Node> DeepClone(IEnumerable<Node> nodes)
	    {
	        var clonedList = new List<Node>();
	        foreach (var node in nodes)
	        {
	            var clonedNode = new Node
	            {
	                NodeId = node.NodeId
	            };
	            foreach (var arc in node.Arcs)
	            {
	                clonedNode.Arcs.Add(new Arc
	                {
	                    ArcId = arc.ArcId,
                        ArcWeights = arc.ArcWeights,
                        Cost = arc.Cost,
                        Destination = new Node
                        {
                            NodeId = arc.Destination.NodeId
                        }
	                });
	            }
                clonedList.Add(clonedNode);
	        }

            return clonedList;
	    }

	    public static List<Path> YensCore(Path shortestPath, Node destination, int K, int[] constraints = null)
	    {
	        var workspace = DeepClone(Program.Nodes);

            var pathList = new List<Path> {shortestPath};
	        var potentialPathList = new List<Path>();

            var k = 1;
            while(k < K)
            {
                if (pathList.Count < k)
                    break;

                for (var i = 0; i < pathList[k - 1].Hops.Count - 1; i++)
                {
                    var sphurNode = pathList[k - 1].Hops[i];
                    var rootPath = pathList[k - 1].Hops.Take(i).ToList();

                    foreach (var path in pathList)
                    {
                        if (Path.IsEquals(rootPath, path.Hops.Take(i).ToList()))
                        {
                            var nextHopNode = pathList[k - 1].Hops[i + 1];
                            // We're assuming that only one link connects two vertices
                            Program.Nodes.FindLast(node => node.NodeId == sphurNode.NodeId).Arcs
                                .RemoveAll(arc => arc.Destination.NodeId == nextHopNode.NodeId);
                        }
                    }

                    foreach (var rootPathNode in rootPath.Where(n => n.NodeId != sphurNode.NodeId).ToList())
                    {
                        workspace.RemoveAll(n => n.NodeId == rootPathNode.NodeId);
                        foreach (var node in workspace)
                            node.Arcs.RemoveAll(a => a.Destination.NodeId == rootPathNode.NodeId);
                    }

                    var sphurPath = Dijkstra(sphurNode, destination);
                    if(sphurPath == null)
                    {
                        break;
                    }
                    var fullPath = rootPath.Concat(sphurPath.Hops).ToList();
                    var tmpPath = new Path
                    {
                        StartingNode = rootPath.FirstOrDefault(),
                        EndNode = destination,
                        Hops = fullPath.ToList()
                    };
                    tmpPath.CalculatePathCost();
                    potentialPathList.Add(tmpPath);

                    workspace = DeepClone(Program.GraphBackup);
                }

                if (!potentialPathList.Any())
                    break;

                potentialPathList.RemoveAll(p => p.PathCost == int.MaxValue || p.PathCost < 0);

                if (constraints != null)
                {
                    FlagOutOfConstraintsPaths(potentialPathList, constraints);
                }

                foreach (var potentialPath in potentialPathList)
                {
                    foreach (var usingPath in pathList)
                    {
                        if (Path.IsEquals(potentialPath.Hops, usingPath.Hops))
                        {
                            potentialPath.PathCost = int.MaxValue;
                        }
                    }
                }

                if (potentialPathList.Count == 0)
                    break;

                potentialPathList = potentialPathList.OrderBy(p => p.PathCost).ToList();

                var willbeUsingPath = potentialPathList.First();
                if (pathList.All(p => p.PathId != willbeUsingPath.PathId))
                {
                    pathList.Add(willbeUsingPath);
                }

                potentialPathList.RemoveAt(0);

                if (potentialPathList.Count == 0)
                    break;

                k++;
            }

            return pathList;
        }

	    public static void FlagOutOfConstraintsPaths(List<Path> paths, int[] constraints)
	    {
            foreach (var p in paths)
            {
                if (CheckIfExeedConstraints(p, constraints))
                    p.PathCost = int.MaxValue;
            }
        }

	    public static bool CheckIfExeedConstraints(Path path, int[] constraints)
	    {
	        return constraints.Where((t, i) => path.PathConstraintCost[i] > t).Any();
	    }

	    public static List<Path> Yens(Node source, Node destination, int K)
	    {
	        var shortestPath = Dijkstra(source, destination);

	        if (shortestPath.StartingNode == shortestPath.Hops.First() && shortestPath.EndNode == shortestPath.Hops.Last() &&
	            shortestPath.Hops.Count == 2)
	            return new List<Path>();

            shortestPath.CalculatePathCost();

	        Console.WriteLine("Wyliczanie podsciezek dla sciezki: " + shortestPath.PathId);

            var tnew = StopWatchHolder._stopWatch.Elapsed;
	        var paths = YensCore(shortestPath, destination, K);

	        Console.WriteLine("Elapsed: " + (StopWatchHolder._stopWatch.Elapsed - tnew));

            Program.Nodes = DeepClone(Program.GraphBackup);
            return paths;
	    }

	    public static List<Path> ConstraintYens(Node source, Node destination, int K, int[] constraints)
	    {
	        var shortestPathList = Yens(source, destination, K);
	        Program.Nodes = DeepClone(Program.GraphBackup);

            FlagOutOfConstraintsPaths(shortestPathList, constraints);
	        var constraintPaths = shortestPathList.ToList();
            constraintPaths.RemoveAll(p => p.PathCost == int.MaxValue);

	        for(var p = 0; p < constraintPaths.Count; p++)
            {
                Console.WriteLine("Wyliczanie podsciezek dla maciezystej sciezki: " + constraintPaths[p].PathId);

                var tnew = StopWatchHolder._stopWatch.Elapsed;
                var foundPaths = YensCore(constraintPaths[p], destination, K, constraints);

                Console.WriteLine("Elapsed: " + (StopWatchHolder._stopWatch.Elapsed - tnew));
                foreach (var fpath in foundPaths)
                {
                    if (constraintPaths.Count >= K)
                        break;

                    if(fpath.PathCost == int.MaxValue)
                        continue;

                    if (constraintPaths.All(path => path.PathId != fpath.PathId))
                        constraintPaths.Add(fpath);
                }
            }

	        constraintPaths = constraintPaths.OrderBy(p => p.PathCost).ToList();
	        return constraintPaths;
	    }

	    private static void CalculateDijkstra(Node source)
		{
            DijkstraData.Clear();
            CheckList.Clear();
			foreach (var node in Program.Nodes)
			{
				DijkstraData.Add(node.NodeId, new Tuple<int, int>(-1, int.MaxValue));
				CheckList.Add(node.NodeId, false);
			}

			var startingNode = source;
			DijkstraData[startingNode.NodeId] = new Tuple<int, int>(startingNode.NodeId, 0);

			while (CheckList.Any(i => i.Value == false))
			{
				var consideredNodes = DijkstraData.Where(v => CheckList[v.Key] == false);
				var minValue = consideredNodes.Min(v => v.Value.Item2);
				var currentNode = Program.Nodes.Find(n => n.NodeId == consideredNodes.Where(p => p.Value.Item2 == minValue).Select(q => q.Key).FirstOrDefault());

				foreach (var arc in currentNode.Arcs)
				{
					if (CheckList[arc.Destination.NodeId])
						continue;

					var currentCostToNode = DijkstraData[arc.Destination.NodeId].Item2;
					if (currentCostToNode > arc.Cost + DijkstraData[currentNode.NodeId].Item2)
					{
						DijkstraData[arc.Destination.NodeId] = new Tuple<int, int>(currentNode.NodeId, arc.Cost + DijkstraData[currentNode.NodeId].Item2);
					}
				}
				CheckList[currentNode.NodeId] = true;
			}
		}

		public static int GetCost(this Dictionary<Tuple<int, int>, int> costs, int source, int destination)
		{
			int cost;
			if (costs.TryGetValue(new Tuple<int, int>(source, destination), out cost))
				return cost;

			costs.TryGetValue(new Tuple<int, int>(destination, source), out cost);
			return cost;
		}

		public static int[] GetWeights(this Dictionary<Tuple<int, int>, int[]> weights, int source, int destination)
		{
			int[] weight;
			if (weights.TryGetValue(new Tuple<int, int>(source, destination), out weight))
				return weight;

			weights.TryGetValue(new Tuple<int, int>(destination, source), out weight);
			return weight;
		}
	}
}