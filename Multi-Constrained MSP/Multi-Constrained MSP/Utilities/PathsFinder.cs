﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Multi_Constrained_MSP.Graph;

namespace Multi_Constrained_MSP.Utilities
{
    public static class PathsFinder
    {
     
        public static List<Path> FindPaths(Node source, Node destination, int pathsNum)
        {
            var pathList = new List<Path>{ BFS(source, destination) };
            var potentialPathList = new List<Path>();
            var backupNode = GraphUtilities.DeepClone(Program.Nodes);

            var k = 1;
            while (k < pathsNum)
            {
                for (var i = 0; i < pathList[k - 1].Hops.Count - 1; i++)
                {
                    var sphurNode = pathList[k - 1].Hops[i];
                    var rootPath = pathList[k - 1].Hops.Take(i + 1).ToList();

                    foreach (var path in pathList)
                    {
                        if (Path.IsEquals(rootPath, path.Hops.Take(i + 1).ToList()))
                        {
                            var nextHopNode = pathList[k - 1].Hops[i + 1];
                            // We're assuming that only one link connects two vertices
                            // Remove for nextHopNode
                            sphurNode.Arcs.RemoveAll(arc => arc.Destination.NodeId == nextHopNode.NodeId);
                            // Remove for sphur node
                            nextHopNode.Arcs.RemoveAll(arc => arc.Destination.NodeId == sphurNode.NodeId);
                        }
                    }

                    var sphurPath = BFS(sphurNode, destination);
                    if(sphurPath == null)
                    {
                        break;
                    }
                    var tempPath = rootPath.Concat(sphurPath.Hops).ToList();
                    for (int j = tempPath.Count - 1; j > 0; j--) {
                        if(tempPath[j - 1].NodeId == tempPath[j].NodeId)
                        {
                            tempPath.RemoveAt(j);
                        }
                    }
                    potentialPathList.Add(new Path
                    {
                        StartingNode = rootPath.FirstOrDefault(),
                        EndNode = destination,
                        Hops = tempPath
                    });

                    source = backupNode.Find(node => node.NodeId == source.NodeId);
                }

                if (!potentialPathList.Any())
                    break;

                pathList.AddRange(potentialPathList.Distinct().Except(pathList));
                potentialPathList.Clear();
                k++;
            }

            Program.Nodes = backupNode;

            Console.WriteLine($"Znalezione potencjalne ścieżki: {String.Join("\n", pathList)}");
            Console.WriteLine();
            return pathList.Take(pathsNum).ToList();
        }

        private static Path BFS(Node source, Node destination)
        {
            // Implements BFS algorithm
            // See: https://pl.wikipedia.org/wiki/Przeszukiwanie_wszerz
            Queue<Node> nodesToExamine = new Queue<Node>();
            Dictionary<int, NodeData> nodesData = new Dictionary<int, NodeData>();
            nodesData.Add(source.NodeId, new NodeData { Visited = true, Parent = null });

            nodesToExamine.Enqueue(source);
            while(nodesToExamine.Any())
            {
                Node examinedNode = nodesToExamine.Dequeue();
                foreach(var arc in examinedNode.Arcs)
                {
                    if(!nodesData.ContainsKey(arc.Destination.NodeId))
                    {
                        nodesData.Add(arc.Destination.NodeId, new NodeData { Visited = true, Parent = examinedNode });
                        nodesToExamine.Enqueue(arc.Destination);
                    }
                }
            }

            return nodesData.ContainsKey(destination.NodeId) ? ConvertToPath(source, destination, nodesData) : null;
        }

        private static Path ConvertToPath(Node source, Node destination, Dictionary<int, NodeData> nodesData)
        {
            var nodesOnPath = new List<Node>();
            nodesOnPath.Add(destination);

            NodeData currentNodeData;
            nodesData.TryGetValue(destination.NodeId, out currentNodeData);
            while (currentNodeData.Parent != null)
            {
                nodesOnPath.Add(currentNodeData.Parent);
                nodesData.TryGetValue(currentNodeData.Parent.NodeId, out currentNodeData);
            }
         
            nodesOnPath.Reverse();
            return new Path { StartingNode = source, EndNode = destination, Hops = nodesOnPath };
        }

        private class NodeData
        {
            public bool Visited;
            public Node Parent;
        }
    }
}
